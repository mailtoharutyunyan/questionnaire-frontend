# Angular7 Spring Boot JWT Starter

## File Structure
```
angular-spring-starter/frontend
 ├──src/
 │   ├──app                                                     * WebApp: folder
 │   │   ├──conponent                                           * stores all the reuseable components
 │   │   │   ├──api-card                                        * the card component in the home page
 │   │   │   ├──footer
 │   │   │   ├──github                                          * github banner in home page
 │   │   │   └──header
 │   │   ├──guard
 │   │   │   ├──login.guard.ts                                  * prevents unauthticated users from going into certain routes
 │   │   │   └──guest.guard.ts                                  * prevents authticated user from going into certain routes. e.g /login
 │   │   ├──home                                                * home dashboard component
 │   │   ├──login                                               * login page card component
 │   │   ├──change-password                                     * change password card component
 │   │   ├──not-found                                           * not found page component
 │   │   ├──service
 │   │   │   ├──api.service.ts                                  * base api service class, the parent class for all api related services
 │   │   │   ├──auth.service.ts                                 * auth related api service like /login /logout
 │   │   │   ├──config.service.ts                               * global api path config file, this service stores all the app related api paths
 │   │   │   ├──foo.service.ts                                  * demo public api service FOO
 │   │   │   └──user.service.ts                                 * service for init user info and view user info
 │   │   │       ├──DeleteableModelRepository.java              * base repository that overwrites the findAll method.
 │   │   │       └──UserRepository.java
 │   │   ├──app-routing.module.ts                               * main router module
 │   │   ├──app.component.*                                     * main app component
 │   │   └──app.module.ts                                       * mian app module
 │   ├──assets                                                  * static files, images etc.
 │   └──environments
 │       ├──environments.prod.ts                                * production env config file
 │       └──environments.ts                                     * develop env config file
 ├──karma.conf.js                                               * karma config for our unit tests
 ├──package.json                                                * what npm uses to manage it's dependencies
 ├──protractor.conf.js                                          * protractor config for our end-to-end tests
 ├──proxy.conf.json                                             * proxy frontend request to backend :8080
 ├──tsconfig.json                                               * typescript config used outside webpack
 └──tslint.json                                                 * typescript lint config
